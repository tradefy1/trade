import webview
from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello():
    return """
    <!DOCTYPE html>
<html>
<head>
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@800&display=swap" rel="stylesheet">
    <title>Bitcoin Price Chart</title>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
</head>
    <style type="text/css">

        body {
            background: #1F0036;
        }

        ::-webkit-scrollbar {
            display: none;
        }
        body {
            overflow: -moz-scrollbars-none;
            -ms-overflow-style: none;
        }

        @keyframes xuyanime {
            0%   {width:0px; }
            100% {width:710px;}
        }

        #xuychik {
            animation-name: xuyanime;
            animation-duration: 60s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
            display: none;
        }

        @keyframes xuyanim {
            0%   {border-right:3px dashed #0C0E15;}
            10%  {border-right:3px dashed #FFF;}
            90%  {border-right:3px dashed #FFF;}
            100% {border-right:3px dashed #0C0E15;}
        }

        .xuy {
            animation-name: xuyanim;
            animation-duration: 2.5s;
            animation-iteration-count: infinite;
        }

        #wrapper {
            position: absolute;
            top: 0;
            left: 0;
            background: #0C0E15;
            border: 1px solid #000;
            box-shadow: 0 22px 35px -16px rgba(0, 0, 0, 0.71);
            max-width: 850px;
            margin: 0 auto;
            height: 475px;
        }

        #chart-bar {
            position: relative;
            margin-top: -38px;
        }

        #chart-area {
            width:760px;
            position: relative;
            bottom:5px;
        }

        .leftbar {
            height: 405px;
            width:70px;
            background-color: #0A0B10;
        }

        .link {
            position: absolute;
            bottom: 7px;
            right: 13px;
            z-index: 10;
            color: #ccc;
            font-size: 12px;
            text-decoration: none;
            font-family: Helvetica, Arial;
        }

        .bar {
            width:100%;
            height:70px;
            background-color:#0A0B10;
            font-family: 'Open Sans', sans-serif;
        }

        .search-input {
            padding: 6px;
            font-size: 12px;
            border: 2px solid #808080; /* Smaller initial border on all sides */
            border-radius: 5px; /* Slight border radius */
            outline: none;
            transition: border 0.3s, box-shadow 0.3s; /* Add box-shadow transition */
            width: 170px;
            background-color: #0A0B10; /* Darker input background */
            color: #ecf0f1; /* Light text color */
            font-family: 'Open Sans', sans-serif;
        }

        .search-input:hover,
        .search-input:focus {
            border: 2px solid #7E5DEC; /* Smaller border on hover/focus */
            box-shadow: 0 0 10px rgba(126, 93, 236, 0.5); /* Shadow on hover */
        }

        *::selection {
            background-color: #7E5DEC; /* Purple color for selected text */
            color: #fff; /* White text color for selected text */
        }
        .search-results {
            position: fixed;
            margin-left: 421px;
            top:60px;
            font-size: 14px;
            background-color: #10131C;
            color: #ecf0f1;
            border-radius: 5px;
            padding: 15px;
            display: none;
            width: 162px;
            font-family: 'Open Sans', sans-serif;
            z-index: 15;
            cursor: pointer;
        }

        .binance:hover {
            opacity: 0.85;
        }

        .binance {
            transform: scale(0.85);
            text-shadow: 0 0 25px rgba(255,192,35);
        }


        .leftbaricons {
            position: relative;
            padding: 2px;
            cursor: pointer;
            height:53px;
            background-color: #10121A;
        }

        .leftbaricons:hover {
            background-color: #12151E;
        }
        
        .bariconactive {
            background-color: #131620;
        }
        .bariconactive:hover {
            background-color: #131620;
        }

        .AI {
            display: block;
        }

        .mine:active {
            background-color: #571E2B;
        }
        .mine {
            transition: 0.1s;
        }
    </style>


<body>
    <div id="wrapper">
        <div class='bar'>
            <img src="https://cdn.discordapp.com/attachments/1063918715428675634/1214576952413782106/darktradefy.png?ex=660c12ef&is=65f99def&hm=d23b27c2b5f0ae634613d85897b1519ec212cb6fd4b6ad75ff5501b399132788&" width='70px' style='user-select: none; cursor: pointer;'> 
            <span style='color:#CCCCC0; font-size: 20px; margin-left:5px;position: fixed; margin-top: 21px; user-select: none; cursor: pointer;'>Tradefy<span style='font-size:10px; color:#B6B6B6; margin-left:92px; position:fixed; bottom: 17px; position: relative;' class="AI">USDT-BTC</span></span>
            <input type="text" class="search-input" style='position: fixed; margin-top: 20px; margin-left:355px' placeholder="Stocks, crypto...">
            <div class="search-results"></div>
            <span class="AI"><span style='position: fixed; position:relative; bottom:49.5px; margin-left:225px; color:#B6B6B6; font-size: 12px; cursor: pointer; user-select: none;' id='timeframe'>1m</span></span>
            <div onclick='window.open("https://accounts.binance.com/en/login?loginChannel=&return_to=aHR0cHM6Ly93d3cuYmluYW5jZS5jb20vZW4vbXkvZGFzaGJvYXJk")' style='color:#F0B90B; background: #181A20; padding:10px; width:180px; text-align: center;border-radius: 5px; position: absolute; margin-left:632px; top:10.5px; user-select: none; cursor: pointer;' class='binance'>
            <svg class='binancesvg' style="color: rgb(240, 185, 11); position:relative; top:2.85px; right:10px;" width='25px' role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
            <path d="M16.624 13.9202l2.7175 2.7154-7.353 7.353-7.353-7.352 2.7175-2.7164 4.6355 4.6595 4.6356-4.6595zm4.6366-4.6366L24 12l-2.7154 2.7164L18.5682 12l2.6924-2.7164zm-9.272.001l2.7163 2.6914-2.7164 2.7174v-.001L9.2721 12l2.7164-2.7154zm-9.2722-.001L5.4088 12l-2.6914 2.6924L0 12l2.7164-2.7164zM11.9885.0115l7.353 7.329-2.7174 2.7154-4.6356-4.6356-4.6355 4.6595-2.7174-2.7154 7.353-7.353z" fill="#f0b90b"></path></svg><span style="position:relative; bottom:3.85px;">Automation</span></div>
        </div>
        <span class="AI">
            <div class='xuy' id='xuychik' style='background:#0C0E15; opacity: 0.7; z-index:3; height:262px; position:fixed; pointer-events: none; margin-top: 2px;margin-left:119px; border-right: 3px dashed #FFF;'></div>
        </span>
        <div style="display:flex; width:835px;">
            <div class="leftbar" style="color:#FFF;">
                <center><div class="leftbaricons bariconactive" id="ai"><svg style='margin-top:6px;' width="40px" height="40px" viewBox="-51.2 -51.2 614.40 614.40" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" fill="#000000" transform="matrix(1, 0, 0, 1, 0, 0)rotate(0)"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round" stroke="#CCCCCC" stroke-width="22.528"></g><g id="SVGRepo_iconCarrier"> <title>ai</title> <g id="Page-1" stroke-width="0.00512" fill="none" fill-rule="evenodd"> <g id="icon" fill="#CCCCC0" transform="translate(64.000000, 64.000000)"> <path d="M320,64 L320,320 L64,320 L64,64 L320,64 Z M171.749388,128 L146.817842,128 L99.4840387,256 L121.976629,256 L130.913039,230.977 L187.575039,230.977 L196.319607,256 L220.167172,256 L171.749388,128 Z M260.093778,128 L237.691519,128 L237.691519,256 L260.093778,256 L260.093778,128 Z M159.094727,149.47526 L181.409039,213.333 L137.135039,213.333 L159.094727,149.47526 Z M341.333333,256 L384,256 L384,298.666667 L341.333333,298.666667 L341.333333,256 Z M85.3333333,341.333333 L128,341.333333 L128,384 L85.3333333,384 L85.3333333,341.333333 Z M170.666667,341.333333 L213.333333,341.333333 L213.333333,384 L170.666667,384 L170.666667,341.333333 Z M85.3333333,0 L128,0 L128,42.6666667 L85.3333333,42.6666667 L85.3333333,0 Z M256,341.333333 L298.666667,341.333333 L298.666667,384 L256,384 L256,341.333333 Z M170.666667,0 L213.333333,0 L213.333333,42.6666667 L170.666667,42.6666667 L170.666667,0 Z M256,0 L298.666667,0 L298.666667,42.6666667 L256,42.6666667 L256,0 Z M341.333333,170.666667 L384,170.666667 L384,213.333333 L341.333333,213.333333 L341.333333,170.666667 Z M0,256 L42.6666667,256 L42.6666667,298.666667 L0,298.666667 L0,256 Z M341.333333,85.3333333 L384,85.3333333 L384,128 L341.333333,128 L341.333333,85.3333333 Z M0,170.666667 L42.6666667,170.666667 L42.6666667,213.333333 L0,213.333333 L0,170.666667 Z M0,85.3333333 L42.6666667,85.3333333 L42.6666667,128 L0,128 L0,85.3333333 Z" id="Combined-Shape"> </path> </g> </g> </g></svg></div></center>
                <center><div class="leftbaricons mine"><svg style='margin-top:16px;' fill="#CCCCC0" width="24px" height="24px" viewBox="0 0 512 512" version="1.1" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" stroke="#CCCCC0"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <g id="Mining_Cart"> <g id="XMLID_888_"> <path d="M358.371,398.16c-18.273,0-33.14,14.867-33.14,33.141c0,18.273,14.867,33.14,33.14,33.14 c18.274,0,33.141-14.867,33.141-33.14C391.512,413.027,376.645,398.16,358.371,398.16z M365.419,438.345 c-1.863,1.852-4.424,2.919-7.054,2.919c-2.62,0-5.181-1.066-7.044-2.919c-1.854-1.853-2.919-4.424-2.919-7.044 c0-2.62,1.065-5.19,2.919-7.044c1.853-1.852,4.424-2.919,7.044-2.919c2.63,0,5.19,1.066,7.054,2.919 c1.852,1.853,2.909,4.424,2.909,7.044C368.328,433.921,367.272,436.492,365.419,438.345z" id="XMLID_891_"></path> <polygon id="XMLID_892_" points="412.531,152 325.531,47.571 260.958,115.1 297.843,152 "></polygon> <polygon id="XMLID_893_" points="180.569,62.905 91.474,152 269.664,152 "></polygon> <path d="M153.629,398.16c-18.274,0-33.141,14.867-33.141,33.141c0,18.273,14.867,33.14,33.141,33.14 c18.273,0,33.14-14.867,33.14-33.14C186.77,413.027,171.902,398.16,153.629,398.16z M160.667,438.345 c-1.852,1.852-4.413,2.919-7.033,2.919c-2.631,0-5.2-1.066-7.054-2.919c-1.853-1.853-2.919-4.424-2.919-7.044 c0-2.62,1.065-5.19,2.919-7.044c1.853-1.852,4.423-2.919,7.054-2.919c2.62,0,5.181,1.066,7.033,2.919 c1.863,1.853,2.929,4.424,2.929,7.044C163.597,433.921,162.53,436.492,160.667,438.345z" id="XMLID_896_"></path> <polygon id="XMLID_897_" points="45.254,271 466.746,271 475.526,227 36.474,227 "></polygon> <path d="M433.82,172c-0.004,0-0.008,0-0.011,0c-0.003,0-0.006,0-0.009,0H67.428c-0.005,0-0.01,0-0.015,0H2v34h508 v-34H433.82z" id="XMLID_898_"></path> <path d="M75.586,422h25.833c4.466-25,26.169-43.632,52.21-43.632c26.04,0,47.743,18.632,52.209,43.632h100.324 c4.466-25,26.169-43.632,52.209-43.632c26.04,0,47.744,18.632,52.21,43.632h25.833l26.341-131H49.245L75.586,422z" id="XMLID_899_"></path> </g> </g> <g id="Layer_1"></g> </g></svg></div></center>
                <center><div class="leftbaricons mine"><svg style='margin-top:14px;' width="24px" height="24px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <path d="M17.1929 20.542C17.4797 21.0139 18.0999 21.1704 18.5438 20.8418C19.7886 19.9206 20.8279 18.7446 21.5901 17.388C22.5159 15.74 23.0015 13.8813 23 11.9911C22.9985 10.1008 22.5099 8.24291 21.5813 6.59646C20.817 5.24112 19.7757 4.06675 18.5295 3.14756C18.0851 2.81973 17.4651 2.97722 17.179 3.44961C16.8929 3.92199 17.0462 4.53563 17.484 4.87241C18.4384 5.60674 19.2384 6.52748 19.8333 7.5823C20.5924 8.92836 20.9919 10.4473 20.9931 11.9927C20.9944 13.5381 20.5974 15.0577 19.8404 16.405C19.2472 17.4608 18.4487 18.3828 17.4955 19.1187C17.0583 19.4562 16.906 20.07 17.1929 20.542Z" fill="#CCCCC0"></path> <path d="M15.1784 17.0884C15.471 17.5568 16.0939 17.7048 16.5159 17.3485C17.1624 16.8026 17.7068 16.1422 18.1201 15.3976C18.6959 14.3605 18.9987 13.1941 19 12.0078C19.0013 10.8216 18.7012 9.6545 18.1277 8.61609C17.716 7.87056 17.1732 7.20893 16.5279 6.66161C16.1067 6.30437 15.4834 6.45097 15.1898 6.91872C14.8961 7.38648 15.0461 7.99795 15.4461 8.37868C15.8144 8.7291 16.1285 9.13501 16.3761 9.58341C16.7856 10.325 16.9999 11.1585 16.999 12.0056C16.998 12.8527 16.7818 13.6857 16.3706 14.4264C16.122 14.8742 15.807 15.2794 15.438 15.629C15.0371 16.0088 14.8858 16.62 15.1784 17.0884Z" fill="#CCCCC0"></path> <path d="M6.80715 3.45801C6.52025 2.98608 5.90008 2.82959 5.45615 3.15815C4.21145 4.07935 3.1721 5.25541 2.40993 6.61198C1.48406 8.25993 0.998473 10.1187 1 12.0089C1.00153 13.8991 1.49013 15.7571 2.41867 17.4035C3.18303 18.7589 4.22428 19.9332 5.47048 20.8524C5.91494 21.1802 6.53486 21.0228 6.82099 20.5504C7.10711 20.078 6.95376 19.4643 6.51604 19.1276C5.56163 18.3932 4.76161 17.4725 4.16673 16.4177C3.40759 15.0716 3.00814 13.5526 3.00689 12.0073C3.00564 10.4619 3.40263 8.94228 4.15958 7.59499C4.75275 6.53921 5.55128 5.61717 6.5045 4.8813C6.94168 4.54381 7.09404 3.92993 6.80715 3.45801Z" fill="#CCCCC0"></path> <path d="M8.82162 6.91159C8.52904 6.44317 7.90607 6.29518 7.48409 6.65147C6.83758 7.19734 6.29322 7.85776 5.87985 8.60237C5.30409 9.63949 5.00133 10.8059 5.00001 11.9921C4.99868 13.1784 5.29882 14.3455 5.87226 15.3839C6.28396 16.1294 6.82683 16.791 7.47211 17.3384C7.8933 17.6956 8.51659 17.549 8.81023 17.0812C9.10386 16.6135 8.95393 16.002 8.55385 15.6213C8.18562 15.2709 7.87155 14.865 7.62393 14.4166C7.21442 13.675 7.00007 12.8415 7.00102 11.9944C7.00197 11.1473 7.21818 10.3143 7.62936 9.57361C7.87798 9.12576 8.19296 8.72056 8.56198 8.37096C8.96291 7.99113 9.11421 7.38 8.82162 6.91159Z" fill="#CCCCC0"></path> <path d="M15 12C15 13.6568 13.6569 15 12 15C10.3431 15 9 13.6568 9 12C9 10.3431 10.3431 8.99998 12 8.99998C13.6569 8.99998 15 10.3431 15 12ZM10.9895 12C10.9895 12.5581 11.4419 13.0105 12 13.0105C12.5581 13.0105 13.0105 12.5581 13.0105 12C13.0105 11.4419 12.5581 10.9894 12 10.9894C11.4419 10.9894 10.9895 11.4419 10.9895 12Z" fill="#CCCCC0"></path> </g></svg></div></center>
                <center><div class="leftbaricons" onclick='window.open("https://t.me/FNHXZ");'><svg style='margin-top:14px;' width="23px" height="23px" viewBox="0 0 48 48" version="1" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 48 48" fill="#CCCCC0"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <path fill="#CCCCC0" d="M44.7,11L36,19.6c0,0-2.6,0-5.2-2.6s-2.6-5.2-2.6-5.2l8.7-8.7c-4.9-1.2-10.8,0.4-14.4,4 c-5.4,5.4-0.6,12.3-2,13.7C12.9,28.7,5.1,34.7,4.9,35c-2.3,2.3-2.4,6-0.2,8.2c2.2,2.2,5.9,2.1,8.2-0.2c0.3-0.3,6.7-8.4,14.2-15.9 c1.4-1.4,8,3.7,13.6-1.8C44.2,21.7,45.9,15.9,44.7,11z M9.4,41.1c-1.4,0-2.5-1.1-2.5-2.5C6.9,37.1,8,36,9.4,36 c1.4,0,2.5,1.1,2.5,2.5C11.9,39.9,10.8,41.1,9.4,41.1z"></path> </g></svg></div></center>
                <center><div class="leftbaricons mine" style="margin-top:120px"><svg fill="#B6B6B6" style='margin-top:15px;' width="22px" height="22px" viewBox="0 0 1920 1920" xmlns="http://www.w3.org/2000/svg"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <path d="M1703.534 960c0-41.788-3.84-84.48-11.633-127.172l210.184-182.174-199.454-340.856-265.186 88.433c-66.974-55.567-143.323-99.389-223.85-128.415L1158.932 0h-397.78L706.49 269.704c-81.43 29.138-156.423 72.282-223.962 128.414l-265.073-88.32L18 650.654l210.184 182.174C220.39 875.52 216.55 918.212 216.55 960s3.84 84.48 11.633 127.172L18 1269.346l199.454 340.856 265.186-88.433c66.974 55.567 143.322 99.389 223.85 128.415L761.152 1920h397.779l54.663-269.704c81.318-29.138 156.424-72.282 223.963-128.414l265.073 88.433 199.454-340.856-210.184-182.174c7.793-42.805 11.633-85.497 11.633-127.285m-743.492 395.294c-217.976 0-395.294-177.318-395.294-395.294 0-217.976 177.318-395.294 395.294-395.294 217.977 0 395.294 177.318 395.294 395.294 0 217.976-177.317 395.294-395.294 395.294" fill-rule="evenodd"></path> </g></svg></div></center>
            </div>
            <div>
                <span class="AI">
                    <div id="chart-area"></div>
                    <div id="chart-bar"></div>
                </span>
            </div>
        </div>
        </div>
    </div>


    <script>
// Список возможных поисковых запросов
let searchList = [
    "Bitcoin (BTC)",
];

// Получаем элементы input и search-results
let searchInput = document.querySelector('.search-input');
let searchResults = document.querySelector('.search-results');

// Добавляем обработчик события 'input'
searchInput.addEventListener('input', function() {
    let input = this.value; // Получаем текущее значение input
    if (input.length >= 2) { // Если введено 3 или более символов
        let matches = searchList.filter(item => item.toLowerCase().includes(input.toLowerCase())); // Ищем совпадения в searchList
        if (matches.length > 0) {
            // Создаем кликабельные элементы для каждого совпадения
            searchResults.innerHTML = matches.map(match => `<a href="#" style='color:#ecf0f1; text-decoration:none;' onclick="selectMatch('${match}')">${match}</a>`).join('<br>');
            searchResults.style.display = 'block'; // Показываем search-results
        } else {
            searchResults.style.display = 'none'; // Скрываем search-results, если нет совпадений
        }
    } else {
        searchResults.style.display = 'none'; // Скрываем search-results, если введено менее 3 символов
    }
});

// Функция для обработки выбора совпадения
function selectMatch(match) {
    searchInput.value = match; // Устанавливаем значение input равным выбранному совпадению
    searchResults.style.display = 'none'; // Скрываем search-results

    // Обновляем данные графика
    updateChartData(match);
}



const currencyCode = 'usd';
const apiUrl = `https://api.coingecko.com/api/v3/coins/bitcoin/market_chart?vs_currency=${currencyCode}&days=1`;
var bitcoinPrices;
var lastPrice = null;

async function getBitcoinPrices() {
    try {
        const response = await fetch(apiUrl);
        const data = await response.json();
        bitcoinPrices = data.prices.map(entry => ({
            timestamp: entry[0],
            price: entry[1]
        }));
        return bitcoinPrices;
    } catch (error) {
        console.error('Error fetching Bitcoin prices:', error);
    }
}

const pricesLast12Hours = [];

getBitcoinPrices().then(prices => {
    for (let i = 1; i <= 12; i++) {
        const hoursAgo = i;
        const timestampXHoursAgo = Date.now() - hoursAgo * 60 * 60 * 1000;
        const indexXHoursAgo = prices.findIndex(entry => entry.timestamp >= timestampXHoursAgo);

        if (indexXHoursAgo !== -1) {
            const priceXHoursAgo = prices[indexXHoursAgo].price;
            pricesLast12Hours.push(parseFloat(priceXHoursAgo.toFixed(2)));
        } else {
            pricesLast12Hours.push(null);
        }
    }
    wholecode();
});

async function wholecode() {
    var bitcoinPrice = bitcoinPrices[bitcoinPrices.length - 1].price;
    if (lastPrice === null) {
        lastPrice = bitcoinPrice;
    }
    var zda = 3000;
    function generateDayWiseTimeSeries(baseval, count) {
        var i = 0;
        var randomTrend = 0;
        var series = [];
        while (i < count) {
            var x = baseval;
            var y;
            zxc = Math.floor(Math.random() * 2) + 1
            if (zxc == 1) {
                y = Math.floor(lastPrice + Math.random() * (10 - randomTrend));    
            }
            else {
                y = Math.floor(lastPrice + Math.random() * (10 + randomTrend));
            }
            if (zxc == 1) {
                randomTrend += 4;
            }

            else {
                randomTrend = randomTrend - 4;
            }

            series.push([x, y]);
            baseval += zda;
            i++;
        }
        lastPrice = series[series.length - 1][1];
        return series;
    }

    var data = generateDayWiseTimeSeries(new Date().getTime(), 20);

    function update() {
        data = generateDayWiseTimeSeries(new Date().getTime(), 20);
        elem.style.display = 'none';
        chart1.updateSeries([{data: data}]);
        chart2.updateSeries([{data: data}]);
        elem.style.display = 'block';
    }

    var timeframes = ['1m', '10m', '30m', '1h', '6h', '12h', '1d', '1w'];
    var indicatortimeframes = [3000, 33000, 90000, 184000, 1100000, 2200000, 4400000, 35800000]
    let timeframe = document.getElementById('timeframe')
    var timeframesArrayNum = 1;
    var indicatortimeframesArrayNum = 1;
    timeframe.onclick = function() {
        if (indicatortimeframesArrayNum == 8) {
            indicatortimeframesArrayNum = 0;
        }
        zda = indicatortimeframes[indicatortimeframesArrayNum]
        if (timeframesArrayNum == 8) {
            timeframesArrayNum = 0;
        }
        indicatortimeframesArrayNum = indicatortimeframesArrayNum + 1
        timeframe.textContent = timeframes[timeframesArrayNum]
        timeframesArrayNum = timeframesArrayNum + 1
        update();

    }
    var options1 = {
        chart: {
            id: "chart2",
            type: "area",
            height: 300,
            foreColor: "#ccc",
            toolbar: {
                show: false
            },
            zoom: {
                enabled: false,
            },
        },
        colors: ["#9E59EC"],
        stroke: {
            width: 2.5
        },
        grid: {
            borderColor: "#202423",
            clipMarkers: false,
            yaxis: {
                lines: {
                    show: true,
                }
            },
            xaxis: {
                lines: {
                    show: true
                }
            }
        },
        dataLabels: {
            enabled: false
        },
        fill: {
            gradient: {
                enabled: true,
                opacityFrom: 0.55,
                opacityTo: 0
            }
        },
        markers: {
            size: 4.8,
            colors: ["#131722"],
            strokeColor: "#AE37BB",
            strokeWidth: 3
        },
        series: [
            {
                name: 'Prediction',
                data: data
            }
        ],
        tooltip: {
            theme: "dark"
        },
        xaxis: {
            type: "datetime",
        },
        yaxis: {
            tickAmount: 7,
        }
    };

    var options2 = {
        chart: {
            id: "chart1",
            height: 115,
            type: "bar",
            foreColor: "#ccc",
            brush: {
                target: "chart2",
                enabled: true
            },
        },
        colors: ["#5A4597"],
        series: [
            {
                data: data
            }
        ],
        stroke: {
            width: 2
        },
        grid: {
            borderColor: "#444"
        },
        markers: {
            size: 0
        },
        xaxis: {
            type: "datetime",
            tooltip: {
                enabled: false
            }
        },
        yaxis: {
            tickAmount: 4,
            min: lastPrice - lastPrice * 1/1000,
            max: lastPrice + lastPrice * 1/1000
        }
    };


    var chart1 = new ApexCharts(document.querySelector("#chart-area"), options1);
    chart1.render();
    var chart2 = new ApexCharts(document.querySelector("#chart-bar"), options2);
    chart2.render();
    var elem = document.getElementById("xuychik");
    elem.style.display = 'block';
timeee = 5000
var access = 1;
    setInterval(async function() {
        data = generateDayWiseTimeSeries(new Date().getTime(), 20);
        elem.style.display = 'none';
        chart1.updateSeries([{data: data}]);
        chart2.updateSeries([{data: data}]);
        elem.style.display = 'block';
        access = 1
    }, 60000);
    var zxcz = 0;
    setInterval(async function() {
    function getRandomInt(max) {
        return Math.floor(Math.random() * max);
    }
    ranum = Math.floor(Math.random() * 2) + 1
    // Получить случайный индекс
    var randomIndex = getRandomInt(data.length);
    if (randomIndex > access) {
        if (ranum == 1) {
            data[randomIndex][1] = data[randomIndex][1] + (Math.floor(Math.random() * 7)+2);
        }
        else {
            data[randomIndex][1] = data[randomIndex][1] - (Math.floor(Math.random() * 5)+2);
        }
        chart1.updateSeries([{data: data}]);
        chart2.updateSeries([{data: data}]);
    }
    else {
        console.log('less')
    }
    access = access + 1
}, 3000);

}

    </script>



<script>
    var mine = document.getElementById('mine');
    var ai = document.getElementsByClassName("AI");
    var idai = document.getElementById("ai");
</script>


</body>
</html>

    """

if __name__ == "__main__":
    # Запускаем Flask сервер в отдельном потоке
    import threading
    t = threading.Thread(target=app.run)
    t.start()

    # Создаем окно с помощью pywebview и открываем в нем Flask сервер
    webview.create_window('Tradefy', 'http://127.0.0.1:5000', width=837, height=485, resizable=False, frameless=True)
    webview.start()


